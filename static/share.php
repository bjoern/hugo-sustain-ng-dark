<?php

$service = isset($_GET['service']) ? $_GET['service'] : false;
$url = isset($_GET['url']) ? $_GET['url'] : false;
$title = isset($_GET['title']) ? $_GET['title'] : false;
$diasporapod = isset($_GET['diasporapod']) ? $_GET['diasporapod'] : false;
$gnusocialpod = isset($_GET['gnusocialpod']) ? $_GET['gnusocialpod'] : false;
$mastodonpod = isset($_GET['mastodonpod']) ? $_GET['mastodonpod'] : false;
$popup = isset($_GET['popup']) ? $_GET['popup'] : 'none';

if(empty($service) || empty($url)) {
	echo 'At least one required variable is empty. You have to define at least service and url';
} else {

	$service = htmlspecialchars($service);
	$diasporapod = htmlspecialchars($diasporapod);
	$gnusocialpod = htmlspecialchars($gnusocialpod);
	$mastodonpod = htmlspecialchars($mastodonpod);
	$url = urlencode(htmlspecialchars($url));
	$title = urlencode(htmlspecialchars($title));

	switch($popup) {
		case "gnusocial":
			$gnusocialpod = validateurl($gnusocialpod);
			header("Location: " . $gnusocialpod . "/notice/new?status_textarea=" . $title . " " . $url . " via @bes@io.schiessle.org");
			die();
		case "mastodon":
			$mastodonpod = validateurl($mastodonpod);
			header("Location: " . $mastodonpod . "/share?text=" . $title . " " . $url . " via @bjoern@mastodon.social");
			die();
		case "diaspora":
			$diasporapod = validateurl($diasporapod);
			header("Location: " . $diasporapod . "/bookmarklet?url=" . $url . "&title=" . $title);
			die();
	}

	if($service === "reddit") {
		header("Location: https://reddit.com/submit?url=" . $url . "&title=" . $title);
		die();
	} elseif($service === "flattr") {
		header("Location: https://flattr.com/submit/auto?user_id=fsfe&url=" . $url . "&title=" . $title);
		die();
	} elseif($service === "hnews") {
		header("Location: https://news.ycombinator.com/submitlink?u=" . $url . "&t=" . $title);
		die();
	} elseif($service === "twitter") {
		header("Location: https://twitter.com/share?url=" . $url . "&text=" . $title . "&via=schiessle");
		die();
	} elseif($service === "facebook") {
		header("Location: https://www.facebook.com/sharer/sharer.php?u=" . $url);
		die();
	} elseif($service === "gplus") {
		header("Location: https://plus.google.com/share?url=" . $url);
		die();
	} else {
		echo 'Social network unknown.';
	}
}

function validateurl($url) {
	if (preg_match('#^https?://#i', $url) === 0) {
		return 'https://' . $url;
	} else {
		return $url;
	}
}
