---
author: "Björn Schießle"
date: 2017-07-29
linktitle: Add a presentation to your Blog
title: add a presentation to your Blog
tags: ["slides"]
slides: true
---


### **Introduction**

Last week I gave a presentation, here are my slides:


<div class="slides" id="unique-id-for-multiple-slides-per-post">
<ul>
<li><img src="/blog/presentation/1.jpg"></li>
<li><img src="/blog/presentation/2.jpg"></li>
<li><img src="/blog/presentation/3.jpg"></li>
<li><img src="/blog/presentation/4.jpg"></li>
</ul>
<span class="button prevButton"></span>
<span class="button nextButton"></span>
</div>	

